#!/bin/bash

# Apply database migrations
echo "Apply database migrations"
python manage.py makemigrations
python manage.py migrate

# Create Default SuperUser
echo "Creating Default SuperUser. Change Password after first login"
python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('admin', 'admin@example.com', 'somespace', first_name='Administrator')"
echo "Super User 'admin' was created with password 'somespace'. You should change it after first login!!!"

# Collect static files
echo "Collecting static files..."
python manage.py collectstatic --noinput

# Start server
echo "Starting server"
python manage.py runserver 0.0.0.0:8000

