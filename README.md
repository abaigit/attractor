## Описание запуска

Для запуска необходимо склонировать репозиторий себе на рабочую машину (команда)<br/>
<b> `git clone https://gitlab.com/abaigit/attractor.git` </b> <br/>
затем перейти в папку проекта <b> `cd attractor` </b> <br/>
внутри необходимо создать файл переменных среды <b>`.env`</b> <br/>

данный файл должен содержать следующие переменные и их значения<br/>
(все данные заполнены дефолтными значениями, 
там где информацию можно менять указано "или <ваша информация>", 
в других случаях лучше оставить):

	SECRET_KEY=<КакоЙ$НиБуДь!!СекРетныЙКЛюч#>
    DEBUG=1
    DB_ENGINE=django.db.backends.postgresql
    DB_TYPE=postgres или <ваша информация>
    DB_DATABASE_NAME=postgres или <ваша информация>
    DB_USERNAME=postgres или <ваша информация>
    DB_PASSWORD=postgres или <ваша информация>
    DB_HOST=db
    DB_PORT=5432
    DEFAULT_FROM_EMAIL=attractortest@zohomail.com
    EMAIL_HOST=smtp.zoho.com
    EMAIL_HOST_PASSWORD=soMeRanDPass3$12
    EMAIL_HOST_USER=attractortest@zohomail.com
    EMAIL_PORT=587
    EMAIL_USE_SSL=False
    EMAIL_USE_TLS=True

После, возможно производитьс запуск командой
<b> `docker-compose up` </b><br/>, находясь в корне проекта

при запуске проекта создается супер-пользователь <b> `admin` </b>
c паролем <b> `somespace` </b> <br/>

он нужен для доступа в админ панель. После первого входа 
рекомендуется сменить пароль встроенной формой.


#### Сервер

Теперь сайт будет доступен по адресу - [http://127.0.0.1:8000](http://127.0.0.1:8000).


#### Информация дополнительная

Документация о [Django](https://docs.djangoproject.com/en/3.1/).

Для работы по API используется [Django Rest Framework documentation](https://www.django-rest-framework.org/).

API авторизация реализована через [REST framework JWT Auth](https://jpadilla.github.io/django-rest-framework-jwt/).

В проекте используется БД - [Postgresql](https://www.postgresql.org/) <br/>

#### Git
Если используете OS Windows, перед скачиванием убедитесь что 
`git config --global core.autocrlf false` иначе в файле entrypoint.sh 
могут быть неверные окончания (CRLF вместо LF)


#### Ликвидация контейнера
`docker-compose down` команда для удаления собранного контейнера


#### Admin
<i><b>Действия выполняются в Админ Панеле </b></i> <br/>
отключить доступ к приложению `Пользователю` - в 
<b>`Users`</b> убрать у пользователя флаг <b>active</b><br/>

чтобы отключить публикацию - в <b>`Traveler > Posts`</b>  
убрать у публикации флаг <b>Показывать публикацию</b><br/>

Убрать отдельные разрешения пользователю (просмотр, создание публикаций и т.д.) - 
в панеле <b>`Users`</b> редактировать поле <b>`Permissions`</b>