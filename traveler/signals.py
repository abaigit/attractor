from django.db.models.signals import post_save
from django.contrib.auth.models import Permission
from django.dispatch import receiver
from django.contrib.auth import get_user_model
from traveler.models import Account


User = get_user_model()


@receiver(post_save, sender=User)
def grant_perms(sender, instance, created, **kwargs):
    if created:
        all_permissions = Permission.objects.filter(content_type__app_label='traveler') #, content_type__model='post'
        instance.user_permissions.set(all_permissions)
        Account.objects.create(user=instance).save()

# post_save.connect(grant_perms, sender=User)