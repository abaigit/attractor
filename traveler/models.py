from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import MinLengthValidator
from django.urls import reverse, reverse_lazy
from ckeditor.fields import RichTextField
from .utils import file_size_validator, defaul_raitings
from django.core.validators import MinLengthValidator


User = get_user_model()


class Tag(models.Model):
    name = models.CharField(verbose_name="Тег", max_length=50, unique=True)

    def save(self, *args, **kwargs):
        self.name = self.name.replace(" ", "").lower()
        super(Tag, self).save(*args, **kwargs)

    def __str__(self):
        return self.name


class Country(models.Model):
    name = models.CharField(verbose_name="Страна", max_length=120, unique=True)
    description = RichTextField(verbose_name="Описание страны", null=True, blank=True)

    def __str__(self):
        return self.name


class Account(models.Model): # todo: попробовать сделать через кастомный класс AbstractUser ...
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tag_subs = models.ManyToManyField(Tag, blank=True, default=None, related_name='tag_mtm')
    country_subs = models.ManyToManyField(Country, blank=True, default=None, related_name='country_mtm')
    user_subs = models.ManyToManyField("self", blank=True, default=None, related_name="subscribers")

    def __str__(self):
        return f"<Account: {self.user.username}>"


class Post(models.Model):
    author = models.ForeignKey(Account, on_delete=models.CASCADE, verbose_name="Пользователь")
    title = models.CharField(verbose_name="Заголовок", max_length=150)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, verbose_name="Страна", null=True)
    content = models.TextField(verbose_name="Содержимое публикации", validators=[MinLengthValidator(3)])
    timestamp = models.DateTimeField(verbose_name="Время", auto_now_add=True)
    tags = models.ManyToManyField(Tag, blank=True, default=None, related_name="post_tag_rel")
    raiting = models.JSONField(verbose_name="Рейтинг", default=defaul_raitings, null=True, blank=True)
    visible = models.BooleanField(verbose_name="Показывать публикацию", default=True)

    def get_absolute_url(self):
        return reverse_lazy("traveler:post-detail", kwargs={"pk": self.id})

    def __str__(self):
        return self.title


class PostImage(models.Model):
    post = models.ForeignKey(Post, verbose_name="Публикация", on_delete=models.CASCADE)
    picture = models.ImageField(verbose_name="Изображение", upload_to="pictures", validators=[file_size_validator],)
    is_main_pic = models.BooleanField(verbose_name="Основное фото", default=False)

    def __str__(self):
        return "Изображение к " + self.post.title


class PostComment(models.Model):
    post = models.ForeignKey(Post, verbose_name="Публикация", on_delete=models.CASCADE)
    author = models.ForeignKey(Account, verbose_name="Автор (клиент)", on_delete=models.CASCADE)
    comment = RichTextField("Комментарий", null=True, blank=True)
    timestamp = models.DateTimeField(verbose_name="Время", auto_now_add=True)

    def __str__(self):
        return "Комментарий на " + self.post.title



