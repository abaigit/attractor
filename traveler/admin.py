from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, User as BaseUser
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _
from .models import (Tag, PostImage, Post, PostComment,
                     Country, Account)


User = get_user_model()


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )


admin.site.unregister(BaseUser)
admin.site.unregister(Group)
admin.site.register(User, CustomUserAdmin)


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    search_fields = ["name"]


class PostImageStacked(admin.StackedInline):
    model = PostImage
    extra = 1
    max_num = 10


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    inlines = [PostImageStacked]
    search_fields = ["name"]
    list_display = ["author", "title", "country", "timestamp"]


@admin.register(PostComment)
class PostCommentAdmin(admin.ModelAdmin):
    pass


@admin.register(Country)
class CountryAdmin(admin.ModelAdmin):
    search_fields = ['name']


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    search_fields = ['user__username']
    autocomplete_fields = ['tag_subs', 'country_subs', 'user_subs']


@admin.register(PostImage)
class PostImageAdmin(admin.ModelAdmin):
    pass