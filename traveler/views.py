from django.shortcuts import render, redirect
from django.db.models import Q
from django.views.generic import *
from django.urls import reverse, reverse_lazy
from django.contrib import messages
from django.contrib.auth import logout, login, authenticate
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import (
    LogoutView as DLogoutView,
)
from django.template.loader import render_to_string
from django.core.mail import send_mail
from django.conf import settings
from django.core.paginator import Paginator
from .models import *
from .forms import PostCreateForm, UserSignupForm


class HomeView(View):
    template_name = 'index.html'

    def get(self, request):
        user = request.user
        context = {}

        if user.is_authenticated:

            account = request.user.account
            context["user_subs_posts"] = {post for account in account.user_subs.all() for post in account.post_set.all()}
            context["tag_subs_posts"] = {post for tag in account.tag_subs.all() for post in tag.post_tag_rel.all()}
            context["country_subs_posts"] = {post for country in account.country_subs.all() for post in country.post_set.all()}

            context["post_recent"] = Post.objects.filter(visible=True).order_by('-timestamp')[:3]
            context["tag_list"] = Tag.objects.all()
            context["account_top"] = list({post.author for post in Post.objects.all().order_by('raiting')})[:5]

            context["posts"] = Post.objects.filter(
                Q(tags__in=user.account.tag_subs.all()) |
                Q(country__in=user.account.country_subs.all()) |
                Q(author__in=[account for account in account.user_subs.all()])
            ).distinct().order_by("-timestamp")

        else:
            context["posts"] = Post.objects.all().order_by('-id')[:10]

        paginator = Paginator(context["posts"], 5)
        page_number = request.GET.get('page')
        page_obj = paginator.get_page(page_number)
        context["page_obj"] = page_obj
        return render(request, self.template_name, context)


class PostCreateView(LoginRequiredMixin, TemplateView):
    login_url = 'traveler:login'
    form_class = PostCreateForm
    template_name = 'post_create.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['country_list'] = Country.objects.all()
        context['tag_list'] = Tag.objects.all()

        context["post_recent"] = Post.objects.filter(visible=True).order_by('-timestamp')[:3]
        context["tag_list"] = Tag.objects.all()
        context["account_top"] = list({post.author for post in Post.objects.all().order_by('raiting')})[:5]

        return context

    def post(self, request, *args, **kwargs):
        title = request.POST.get("title")

        country_name = request.POST.get("country")
        country = Country.objects.get_or_create(name=country_name)[0]
        description = request.POST.get("country_des")
        country.description = description
        country.save()

        content = request.POST.get("content")
        files_list = request.FILES.getlist('post_images')
        post_tags = request.POST.get("tags")
        author = request.user.account

        post = Post.objects.create(author=author, title=title, country=country, content=content)

        for img in files_list:
            PostImage.objects.create(post=post, picture=img).save()

        tags = [Tag.objects.get_or_create(name=tag)[0] for tag in post_tags.split()]
        post.tags.set(tags)

        return redirect(reverse("traveler:post-detail", kwargs={"pk": post.id}))


class PostDetailView(LoginRequiredMixin, DetailView):
    login_url = 'traveler:login'
    model = Post
    template_name = 'post_details.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["post_recent"] = Post.objects.filter(visible=True).order_by('-timestamp')[:3]
        context["tag_list"] = Tag.objects.all()
        context["account_top"] = list({post.author for post in Post.objects.all().order_by('raiting')})[:5]
        return context

    def post(self, request, *args, **kwargs):
        this = self.get_object()
        this.delete()
        return redirect(reverse("traveler:home"))


class DashView(LoginRequiredMixin, TemplateView):
    template_name = 'user_dashboard.html'
    login_url = 'traveler:login'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['country_list'] = Country.objects.all()
        context['tag_list'] = Tag.objects.all()
        context['user_list'] = Account.objects.filter(~Q(user=self.request.user))
        context['user'] = User.objects.get(id=self.request.user.id)

        context["post_recent"] = Post.objects.filter(visible=True).order_by('-timestamp')[:3]
        context["tag_list"] = Tag.objects.all()
        context["account_top"] = list({post.author for post in Post.objects.all().order_by('raiting')})[:5]

        return context

    def post(self, request):
        data = request.POST

        tag_list = data.getlist("tag")
        country_list = data.getlist("country")
        user_list = data.getlist("user")

        tag_subs = [Tag.objects.get(name=tag) for tag in tag_list]
        country_subs = [Country.objects.get(name=country) for country in country_list]
        user_subs = [Account.objects.get(id=user) for user in user_list]

        request.user.account.tag_subs.set(tag_subs)
        request.user.account.country_subs.set(country_subs)
        request.user.account.user_subs.set(user_subs)

        messages.info(request, "Ваши настройки сохранены")
        return redirect(reverse_lazy("traveler:dash"))


class CountryListView(LoginRequiredMixin, ListView):
    login_url = 'traveler:login'
    template_name = 'country_list.html'
    context_object_name = "country_list"
    model = Country
    paginate_by = 3

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["post_recent"] = Post.objects.filter(visible=True).order_by('-timestamp')[:3]
        context["tag_list"] = Tag.objects.all()
        context["account_top"] = list({post.author for post in Post.objects.all().order_by('raiting')})[:5]
        return context

    def get_queryset(self):
        return [cntr for cntr in Country.objects.all() if cntr.post_set.filter(visible=True).count() >= 1]


class CountryDetailView(LoginRequiredMixin, DetailView):
    login_url = reverse_lazy('traveler:login')
    template_name = 'country_detail.html'
    context_object_name = 'country'
    model = Country

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["country_posts"] = reversed(self.get_object().post_set.all())

        context["post_recent"] = Post.objects.filter(visible=True).order_by('-timestamp')[:3]
        context["tag_list"] = Tag.objects.all()
        context["account_top"] = list({post.author for post in Post.objects.all().order_by('raiting')})[:5]

        return context


class AccountListView(LoginRequiredMixin, ListView):
    template_name = 'account_list.html'
    context_object_name = "account_data"
    login_url = 'traveler:login'
    model = Account
    # paginate_by = 3

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["post_recent"] = Post.objects.filter(visible=True).order_by('-timestamp')[:3]
        context["tag_list"] = Tag.objects.all()
        context["account_top"] = list({post.author for post in Post.objects.all().order_by('raiting')})[:5]
        return context

    def get_queryset(self):
        aqs = {}
        authors = Account.objects.all()
        for author in authors:
            aqs.update({author: Post.objects.filter(author=author)})
        return aqs


class AccountDetailView(LoginRequiredMixin, DetailView):
    login_url = 'traveler:login'
    template_name = 'account_details.html'
    context_object_name = "account"
    model = Account

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        account = self.get_object()
        context["account_posts"] = Post.objects.filter(Q(visible=True), Q(author=account)).order_by('country')
        context["post_recent"] = Post.objects.filter(visible=True).order_by('-timestamp')[:3]
        context["tag_list"] = Tag.objects.all()
        context["account_top"] = list({post.author for post in Post.objects.all().order_by('raiting')})[:5]
        return context


class LoginView(View):
    template_name = 'login.html'

    def get(self, request):
        if request.user.is_authenticated:
            return redirect("traveler:home")
        return render(request, self.template_name, {})

    def post(self, request):
        username = request.POST.get("username")
        password = request.POST.get("password")
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            messages.success(request, f"Добро пожаловать обратно, {username}")
        else:
            messages.warning(request, "Логин или Пароль неверны")
        return redirect(request.META.get("HTTP_REFERER"))


class LogoutView(DLogoutView, LoginRequiredMixin):
    login_url = 'traveler:login'
    next_page = 'traveler:login'


class SignupView(CreateView):
    template_name = 'signup.html'
    form_class = UserSignupForm
    queryset = User.objects.all()
    success_url = reverse_lazy('traveler:home')


    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST, request.FILES)
        if form.is_valid():
            first_name = form.data.get("first_name")
            username = form.data.get("username")
            password = form.data.get("password1")
            email = form.data.get("email")

            # maybe use it for Account
            # first_name = form.data.get('first_name')
            # last_name = form.data.get("last_name")
            # birth_date = form.data.get("birth_date")
            # photo = form.files.get('photo')

            user = User.objects.create(
                first_name=first_name,
                username=username,
                email=email,
            )
            user.set_password(password)
            user.save()
            messages.success(request, message=f"Congratulations {username}. Your Profile Created succesefully")

            html_email = render_to_string('registration_success_email.html', {"name": username})
            send_mail(f"Traveler. Welcome to your profile {username}",
                      "",
                      settings.EMAIL_HOST_USER,
                      [email],
                      fail_silently=False, html_message=html_email)

            return redirect('traveler:home')
        return render(request, self.template_name, {"form": form})



