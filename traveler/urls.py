from django.urls import path
from . import views

app_name = 'traveler'

urlpatterns = [
    path('', views.HomeView.as_view(), name="home"),
    path('dash/', views.DashView.as_view(), name="dash"),
    path('post/create/', views.PostCreateView.as_view(), name="post-create"),
    path('post/<int:pk>/', views.PostDetailView.as_view(), name="post-detail"), # needs to be removed
    path('country/', views.CountryListView.as_view(), name="country-list"),
    path('country/<int:pk>/', views.CountryDetailView.as_view(), name="country-detail"),
    path('login/', views.LoginView.as_view(), name="login"),
    path('signup/', views.SignupView.as_view(), name="signup"),
    path('logout/', views.LogoutView.as_view(), name="logout"),
    path('account/', views.AccountListView.as_view(), name="account-list"),
    path('account/<int:pk>/', views.AccountDetailView.as_view(), name="account-detail"), # todo: change 'pk' to 'username'
]