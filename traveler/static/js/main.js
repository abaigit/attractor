$(document).ready(function () {
    $("#togglerBtn").click((e) => {
        e.preventDefault();
        let navbar = $("#navbarSupportedContent");
        navbar.hasClass("show") ? navbar.removeClass("show") : navbar.addClass("show");
    });

    $("#myModal").modal('show');
    setTimeout(() => $("#myModal").modal('hide'), 2000)
});