$(document).ready(function () {

    // Post sliders
    $('.toSlick').slick({
        arrows: false,
        mobileFirst: true,
    });

    // Side Menu
    $(".menu-btn").click(()=> $("#mySidenav").width("15%"));
    $(".closebtn").click(()=> $("#mySidenav").width(0) );

    $(".menu-link").click((e) => {
        let block = $(e.target).data("filter");
        $(".posts-block").addClass("visually-hidden");
        $(`.`+block).removeClass("visually-hidden");
    })

});






