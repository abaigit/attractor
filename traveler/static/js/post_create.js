$(document).ready(async () => {

    let csrftoken = Cookies.get("csrftoken");

    const getCountryList = async () => {
        let result;
        await $.ajax({
            url: `https://restcountries.eu/rest/v2/all?fields=name;capital;alpha3Code;subregion;population;nativeName;flag;cioc;`,
            data: {token: csrftoken},
            type: "GET",
            crossDomain: true,
            success: (response) => {
                result = response;
            },
        });
        return await result;
    }

    let counList = await getCountryList();

    counList.forEach((item, ind) => {
        $("#datalistOptions").append(`
            <option value="${item.name}" key="${item.flag}">${item.capital}</option>
        `);
    });

    $("#countryList").change((e) => {
        let textarea = $("#countryDescription");
        textarea.val("");
        let countryInfo = counList.filter(country => country.name === $(e.currentTarget).val());
        for (let [key, val] of Object.entries(countryInfo[0])) {
            let curr = textarea.val();
            textarea.val(curr + `${key} - ${val}\r`);
        }
    });

});

