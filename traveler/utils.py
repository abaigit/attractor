from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

def file_size_validator(value):
    # val = lim if lim else 3
    limit = 5 * 1024 * 1024 # needs to be a dynamic val
    if value.size > limit:
        raise ValidationError(_(f"Ваш файл слишком большой. Размер файла не должен превышать {5} МБ."))

def defaul_raitings():
    return {"sum": 0, "dis": 0}
