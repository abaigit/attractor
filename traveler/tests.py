from django.contrib.auth import get_user_model
from django.contrib.auth.models import Permission
from django.test import TestCase
from traveler.models import Account


User = get_user_model()


class UserTestCase(TestCase):

    def setUp(self) -> None:
        self.user = User(username="mike", email="cek@mak.com", is_staff=True, is_superuser=True)
        self.user.set_password("testPass_using")
        self.user.save()

    def test_user_created(self):
        user_qs = User.objects.all().count()
        self.assertEqual(user_qs, 1)
        self.assertNotEqual(user_qs, 0)

    def test_user_perms_granted(self):
        all_permissions = Permission.objects.filter(content_type__app_label='traveler')
        self.assertEqual(list(self.user.user_permissions.all()), list(all_permissions))

    def test_add_post_perm_block(self):
        add_post = Permission.objects.get(codename="add_post")
        self.user.user_permissions.remove(add_post)
        self.assertNotIn(add_post, self.user.user_permissions.all())


class AccountTestCase(TestCase):

    def setUp(self) -> None:
        self.user = User(username="john", email="cek@mak.com", is_staff=True, is_superuser=True)
        self.user.set_password("testPass_using")
        self.user.save()

    def test_account_created(self):
        self.assertTrue(Account.objects.filter(user=self.user).exists())
