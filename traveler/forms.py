from django.forms import forms, ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import get_user_model
from .models import *


User = get_user_model()


class PostCreateForm(ModelForm):
    class Meta:
        model = Post
        fields = '__all__'
        exclude = ["raiting", "timestamp", "tags"]


class UserSignupForm(UserCreationForm):
    class Meta:
        model = User
        fields = ["first_name", 'username', 'email', 'password1', 'password2']