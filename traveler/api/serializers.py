from django.contrib.auth import get_user_model
from django.db.models import Q
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.fields import CurrentUserDefault
from traveler.models import (Tag, Country, Account,
                             Post, PostImage, PostComment)


User = get_user_model()


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = "__all__"


class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields = "__all__"


class UserSignupSerializer(serializers.ModelSerializer):
    password = serializers.CharField(style={'input_type':'password', 'placeholder': 'The Password please'}, label="Password", write_only=True)
    password_conf = serializers.CharField(style={'input_type': 'password', 'placeholder': 'Repeat Password please'}, label="Confirm Password", write_only=True)

    class Meta:
        fields = ['first_name', 'username', 'email', 'password', 'password_conf']
        model = User

    def create(self, validated_data):
        user = User.objects.create(username=validated_data['username'])
        user.set_password(validated_data['password'])
        user.save()
        return validated_data

    def validate(self, data):
        username = data['username']
        password = data['password']
        password2 = data['password_conf']
        user_qs = User.objects.filter(Q(username=username)).distinct()
        if password != password2:
            raise ValidationError("Пароли не совпадают")
        elif user_qs.count() > 0:
            raise ValidationError("Этот логин уже занят")
        return data


class AccountSerializer(serializers.ModelSerializer):
    user = serializers.SerializerMethodField()
    class Meta:
        model = Account
        fields = "__all__"

    def get_user(self, obj):
        return obj.user.username


class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = "__all__"


class PostListCreateSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        exclude = ['country', 'timestamp', 'tags', 'visible', 'author']

    def save(self, **kwargs):
        account = self.context['request'].user.account
        title = self.validated_data['title']
        content = self.validated_data['content']

        return Post.objects.create(author=account, title=title, content=content).save()


class PostImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostImage
        fields = "__all__"


class PostCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = PostComment
        fields = "__all__"