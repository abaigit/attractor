from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token
from . import views

app_name = 'api'

urlpatterns = [
    path('country/', views.CountryListAPIView.as_view(), name="country-list"),
    path('country/<int:pk>/', views.CountryRUDAPIView.as_view(), name="country-rud"),
    path("post/", views.PostListAPIView.as_view(), name="post-list"),
    path("post/<int:pk>/", views.PostRUDAPIView.as_view(), name="post-rud"),
    path("comment/", views.PostCommentListAPIView.as_view(), name="comment-list"),
    path("comment/<int:pk>/", views.PostCommentRUDAPIView.as_view(), name="comment-rud"),
    path("picture/", views.PostImageListAPIView.as_view(), name="postimage-list"),
    path("picture/<int:pk>/", views.PostImageRUDAPIView.as_view(), name="postimage-rud"),

    # authorization
    path('account/', views.AccountListAPIView.as_view(), name="account-list"),
    path('account/signup/', views.UserSignupAPIView.as_view(), name="signup"),
    path('account/token/', obtain_jwt_token, name="token"),
    path('account/<int:pk>/', views.AccountRUDAPIView.as_view(), name="account-rud")
]