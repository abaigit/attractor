from rest_framework.generics import (ListCreateAPIView, RetrieveUpdateDestroyAPIView,
                                     ListAPIView, CreateAPIView)
from rest_framework.permissions import AllowAny, IsAdminUser
from traveler.models import (Tag, Country, Account,
                             Post, PostImage, PostComment)
from .serializers import (TagSerializer, CountrySerializer,
                          AccountSerializer, PostSerializer, PostListCreateSerializer,
                          PostImageSerializer, PostCommentSerializer,
                          UserSignupSerializer)

from django.contrib.auth import get_user_model


User = get_user_model()


class CountryListAPIView(ListCreateAPIView):
    serializer_class = CountrySerializer
    queryset = Country.objects.all()


class CountryRUDAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = CountrySerializer
    queryset = Country.objects.all()


class UserSignupAPIView(CreateAPIView):
    serializer_class = UserSignupSerializer
    queryset = User.objects.all()
    permission_classes = [IsAdminUser]


class AccountListAPIView(ListAPIView):
    serializer_class = AccountSerializer
    queryset = Account.objects.all()

class AccountRUDAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = AccountSerializer
    queryset = Account.objects.all()


class PostListAPIView(ListCreateAPIView):
    serializer_class = PostListCreateSerializer
    queryset = Post.objects.all()


class PostRUDAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class PostImageListAPIView(ListCreateAPIView):
    serializer_class = PostImageSerializer
    queryset = PostComment.objects.all()


class PostImageRUDAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = PostImageSerializer
    queryset = PostComment.objects.all()

class PostCommentListAPIView(ListCreateAPIView):
    serializer_class = PostCommentSerializer
    queryset = PostComment.objects.all()


class PostCommentRUDAPIView(RetrieveUpdateDestroyAPIView):
    serializer_class = PostCommentSerializer
    queryset = PostComment.objects.all()



