# Generated by Django 3.1.7 on 2021-04-13 05:59

import ckeditor.fields
from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import traveler.utils


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Account',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
        ),
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=120, unique=True, verbose_name='Страна')),
                ('description', ckeditor.fields.RichTextField(blank=True, null=True, verbose_name='Описание страны')),
            ],
        ),
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=150, verbose_name='Заголовок')),
                ('content', models.TextField(validators=[django.core.validators.MinLengthValidator(3)], verbose_name='Содержимое публикации')),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name='Время')),
                ('raiting', models.JSONField(blank=True, default=traveler.utils.defaul_raitings, null=True, verbose_name='Рейтинг')),
                ('visible', models.BooleanField(default=True, verbose_name='Показывать публикацию')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='traveler.account', verbose_name='Пользователь')),
                ('country', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='traveler.country', verbose_name='Страна')),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=50, unique=True, verbose_name='Тег')),
            ],
        ),
        migrations.CreateModel(
            name='PostImage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('picture', models.ImageField(upload_to='pictures', validators=[traveler.utils.file_size_validator], verbose_name='Изображение')),
                ('is_main_pic', models.BooleanField(default=False, verbose_name='Основное фото')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='traveler.post', verbose_name='Публикация')),
            ],
        ),
        migrations.CreateModel(
            name='PostComment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('comment', ckeditor.fields.RichTextField(blank=True, null=True, verbose_name='Комментарий')),
                ('timestamp', models.DateTimeField(auto_now_add=True, verbose_name='Время')),
                ('author', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='traveler.account', verbose_name='Автор (клиент)')),
                ('post', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='traveler.post', verbose_name='Публикация')),
            ],
        ),
        migrations.AddField(
            model_name='post',
            name='tags',
            field=models.ManyToManyField(blank=True, default=None, related_name='post_tag_rel', to='traveler.Tag'),
        ),
        migrations.AddField(
            model_name='account',
            name='country_subs',
            field=models.ManyToManyField(blank=True, default=None, related_name='country_mtm', to='traveler.Country'),
        ),
        migrations.AddField(
            model_name='account',
            name='tag_subs',
            field=models.ManyToManyField(blank=True, default=None, related_name='tag_mtm', to='traveler.Tag'),
        ),
        migrations.AddField(
            model_name='account',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='account',
            name='user_subs',
            field=models.ManyToManyField(blank=True, default=None, related_name='_account_user_subs_+', to='traveler.Account'),
        ),
    ]
